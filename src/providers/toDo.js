import React, { createContext, useContext, useReducer } from "react";
import types from "./types";

const ToDoContext = createContext();
const initialState = [
    {
        id: 1,
        description:'first task',
        state: 1,
        createdAt:'2023-01-19',
        endAt:'2023-01-20'
    },
    {
        id: 2,
        description:'second task',
        state: 2,
        createdAt:'2023-01-19',
        endAt:'2023-01-20'
    },
    {
        id: 3,
        description:'third task',
        state: 3,
        createdAt:'2023-01-19',
        endAt:'2023-01-20'
    }
]

const useToDoContext = () => {
    return useContext(ToDoContext)
}

const toDoReducer = (state, action) => {
    switch(action.type) {
        case types.createToDo:{
            return [
                ...state,
                action.payload
            ]
        }
        case types.modifyToDo:{
            const copyState= [...state]
            copyState.forEach(element => {
                if(element.id === action.payload.id){
                    element.description = action.payload.description
                }
            });
            return copyState
        }
        case types.updateStateToDo:{
            const copyState= [...state]
            copyState.forEach(element => {
                if(element.id === action.payload.id){
                    element.state = action.payload.state
                }
            });
            return copyState
        }
        default:
            return state;
    }
}

const ToDoProvider = ({children}) => {
    const [state, dispatch] = useReducer(toDoReducer, initialState)

    return <ToDoContext.Provider value={{ toDoList:state, dispatch }}>
        { children }
    </ToDoContext.Provider>
}

export { ToDoProvider, useToDoContext }