const types = {
    createToDo: 'to-do-create',
    modifyToDo: 'to-do-modify',
    updateStateToDo: 'to-do-update'
}

export default types;