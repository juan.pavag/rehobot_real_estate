import * as React from 'react';
import { DataGrid } from '@mui/x-data-grid';

const BasicTable = props => {
    const { data, headers } = props  

    return <>
        <DataGrid
            columns= {headers}
            rows= {data}
            getRowId= {row => row.id}
            pageSize= {10}
            rowsPerPageOptions= {[5, 10]}
        />
    </>
}

export { BasicTable }