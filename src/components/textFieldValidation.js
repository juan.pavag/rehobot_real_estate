import React from 'react';
import { styled, TextField, Typography } from '@mui/material';

const TextFieldValidationInput = styled(TextField)({
  marginTop: '8px',
  marginBottom: '8px'

})

const TypographyErrorValidation = styled(Typography)({
  marginTop: '0.25rem',
  marginBottom: '0.25rem',
  fontSize: '.7em',
  color: '#dc3545'
})

const TexfieldValidation = props => {
    const {
        id,
        label= id,
        type= 'text',
        required= false,
        shemaValidation= [],
        errors,
        register
    } = props
    
    return<>
        <TextFieldValidationInput
            fullWidth
            required= {required}
            type= {type}
            label= {label}
            id= {id}
            error= {errors[id]?true:false}
            {...register(id, shemaValidation[id])}
        />
        {errors[id] && <TypographyErrorValidation>
            {errors[id].message}
            </TypographyErrorValidation>
        }
    </>
}

export { TexfieldValidation }
