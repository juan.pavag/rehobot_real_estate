
import React from 'react';
import { styled, Dialog, DialogTitle, DialogContent, Typography, Divider, IconButton } from '@mui/material';

/**
 * 
 * @param {open} is the flag to open the modal.
 * @param {handleClose} is the function that closes the modal
 * @param {title} is the title of the modal.
 * @param {content} is the set of jsx tags to be displayed.
 * @param {width} the width of the modal, since the height depends on the content.
 * 
 * @returns to modal
 */
const ModalDialog = props =>  {
  const {
    open = true,
    handleClose,
    title = 'Título',
    content,
    width='480px'
  } = props;

  return (
    <Dialog
      open={open}
      onClose={() => handleClose()}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
      width={width}
    >
      <DialogTitleModal width={width}>
        <Typography>
          {title}
        </Typography>
        <IconButtonModal 
          onClick={() => handleClose()}
        >X</IconButtonModal>
      </DialogTitleModal> 
      <Divider />
      <DialogContentModal>
        {content && content}
      </DialogContentModal>
    </Dialog>
  );
}

const DialogTitleModal = styled(DialogTitle)({
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'start',
    justifyContent: 'space-between'

})
const DialogContentModal = styled(DialogContent)({
    marginTop:'0px'
})
const IconButtonModal = styled(IconButton)({
  fontWeight: '16px',
  padding:'0px'
})

export { ModalDialog };
