import './App.css';
import {
  BrowserRouter as Router,
  Route,
  Routes
} from 'react-router-dom';

import Index from './views/index';
import RockPaperScissors from './views/app/game/rockPaperScissors'
import NotFound from './views/notFound';

function App() {
  return (
    <div className="App">
      <Router>
        <Routes>
          <Route path="/" element={ <Index /> } />
          <Route path="/game" element={ <RockPaperScissors /> } />
          <Route path="*" element={ <NotFound /> } />
        </Routes>
      </Router>
    </div>
  );
}

export default App;