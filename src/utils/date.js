/** NOW -> [2023,3,4] */
export const currentTime = () => {
    const fecha = new Date();
    const dia = fecha.getDate();
    const mes = fecha.getMonth() + 1;
    const año = fecha.getFullYear()
    return [año, mes, dia]
}

/** [2023,3,4] -> '2023-3-4' */
export const arrangeDate1 = (datos) => {
    let newD = ""
    if (Object.keys(datos).length !== 0) {
        newD = `${datos[0]}-${datos[1]}-${datos[2]}`
    }
    return newD
}

/** '2023-3-4' -> '2023-03-04' */
export const arrangeDate2 = (date) => {
    const arrFechas = date.split('-')
    let fecha = ""
    arrFechas.map(item => {
        if (item.length < 2)
            fecha += "0" + item + "-";
        else
            fecha += item + "-";

        return ""
    })
    fecha = fecha.substring(0, fecha.length - 1);
    return fecha
}