import React from 'react';
import { Snackbar } from '@mui/material';
import MuiAlert from '@mui/material/Alert';

const Alert = React.forwardRef(function Alert(props, ref) {
    return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});

/**
 * 
 * @param {type} severity =>  success, error, warning, info
 * @returns 
 */
const Notification = props => {
    const {
        openAlert,
        setOpenAlert,
        type= 'success',
        mssg= 'Successful process',
        duration=6000
    } = props
     
    const handleClose = (event, reason) => {
        if (reason === 'clickaway') return;
    
        setOpenAlert(false);
      };
    
    return <>
        <Snackbar open={openAlert} autoHideDuration={duration} onClose={handleClose}>
            <Alert onClose={handleClose} severity={type} sx={{ width: '100%' }}>
                {mssg}
            </Alert>
        </Snackbar>
    </>
}

export { Notification }