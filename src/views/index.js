import React from 'react';
import { Box, styled } from '@mui/material';

import { ToDoProvider } from '../providers/toDo';
import { toDoApp } from './app/toDoList/index';

const Home = styled(Box)({
    minWidth: 275,
    width: '100vw',
    height: '100vh',
    backgroundColor: '#CAD2C5'
})

const MainBox = styled(Box)({
    height: '100%',
    marginLeft:'15%',
    marginRight:'15%',
    backgroundColor: '#b3b3b3',

    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'start',
    alignItems: 'start'
})

const Index = () => {
    return <ToDoProvider>
        <Home>
            <MainBox>
                <toDoApp.Index/>
            </MainBox>
        </Home>
    </ToDoProvider>
}

export default Index