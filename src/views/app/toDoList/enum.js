const stateTypes = {
    'TODO': 1,
    'CANCELD': 2,
    'FINISHED': 3
}

const changeNumberText = new Map()
changeNumberText.set(1, 'TODO')
changeNumberText.set(2, 'CANCELD')
changeNumberText.set(3, 'FINISHED')

export { stateTypes, changeNumberText }