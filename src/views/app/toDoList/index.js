import React, { useState } from "react";

import { Grid, styled, IconButton, Typography } from '@mui/material';
import AddIcon from '@mui/icons-material/Add';

import { useToDoContext } from '../../../providers/toDo';
import { ModalDialog } from '../../../components/modalDialog';
import { BasicTable } from '../../../components/table';
import { Create } from './create';
import { Modify, Canceled, Complete, Enable } from './actions';
import { changeNumberText } from './enum'; 

const SetGrid = styled(Grid)({
    width: '100%',
    height: '100%'
})

const FormActions = ({cell}) => {
    return <>
        <Modify cell={cell} />
        <Canceled cell={cell} />
        <Complete cell={cell} />
        <Enable cell={cell} />
    </>
}

const arrangeData = data => {

    const newData = []

    data.forEach(task => {
        const stateText = changeNumberText.get(task.state)
        const data = {
            ...task,
            stateText
        }
        newData.push(data)
    })

    return newData
}

const Index = () => {
    const { toDoList } = useToDoContext();
    const [addModal, setAddModal] = useState(false)

    const closeModal = () => setAddModal(false)


    const columns = [
        {field:'id', headerName:'Id', width:20},
        {field:'description', headerName:'Description', width:150},
        {field:'stateText', headerName:'State', width:120},
        {field:'createdAt', headerName:'CreatedAt', width:120},
        {field:'endAt', headerName:'endAt', width:120},
        {
            field:'actions',
            headerName:'Actions',
            width:180,
            renderCell: params => <FormActions cell={params.row} />
        }
    ] 
   
    return  <SetGrid>
        <IconButton 
            aria-label="ADD to post"
            onClick={() => setAddModal(true)}
        >
            <AddIcon />
            <Typography>Add</Typography>
        </IconButton>
        <BasicTable
            data= {arrangeData(toDoList)}
            headers= {columns}
        />
        <ModalDialog
            open= {addModal}
            handleClose={closeModal}
            title= "Creating TO-DO"
            content= {<Create closeModal={closeModal}/>}
        />
    </SetGrid>
}

export const toDoApp ={ Index }