import React, { useState } from 'react';

import { styled, Button } from '@mui/material';
import { useForm } from 'react-hook-form';

import { TexfieldValidation } from '../../../components/textFieldValidation';
import { useToDoContext } from '../../../providers/toDo';
import types from '../../../providers/types';
import { currentTime, arrangeDate1, arrangeDate2 } from '../../../utils/date';
import { stateTypes } from './enum';
import { Notification } from '../../../utils/notification';

const InputButton = styled(Button)({
    marginTop: '16px',
})

const Create = ({closeModal}) => {
    const { register, handleSubmit, reset, formState: { errors } } = useForm();
    const { toDoList, dispatch } = useToDoContext()
    const [openNotification, setOpenNotification] = useState(false)
    const [typeNotification, setTypeNotification] = useState('success')
    const [mssgNotification, setMssgNotification] = useState('Successful process')

    const toDoShema = {
        description: {
            required: { value: true, message: 'Description required' },
            minLength: { value: 2, message: 'Should have a min of 2 characters' },
            maxLength:{ value: 300, message: 'Should have a max of 300 characters'}
        },
        endAt: {
            required: { value: true, message: 'EndAt required' }
        },
    };

    const onSubmit = data => {
        const currentDate = arrangeDate2(arrangeDate1(currentTime()))

        if(currentDate > data.endAt){
            setTypeNotification('error')
            setMssgNotification('The termination date cannot be less than the current day')
            setOpenNotification(true)
            return
        }

        const todo = {
            id: toDoList.length + 1,
            description: data.description,
            state: stateTypes.TODO,
            createdAt: currentDate,
            endAt: data.endAt
        }

        dispatch({
            type: types.createToDo,
            payload: todo
        })

        reset();
        closeModal()
    }

    return <>
        <form onSubmit={handleSubmit(onSubmit)}>
            <TexfieldValidation
                required
                id= "description"
                label= "Description"
                shemaValidation= {toDoShema}
                errors= {errors}
                register= {register}
            />
            <TexfieldValidation 
                required
                id= "endAt"
                label= "EndAt"
                type= "date"
                shemaValidation= {toDoShema}
                errors= {errors}
                register= {register}
            />
            <InputButton size="small" variant="outlined" color="success" type="submit" >ADD</InputButton>
        </form>
        <Notification 
            openAlert= {openNotification} 
            setOpenAlert= {setOpenNotification}
            type= {typeNotification}
            mssg= {mssgNotification}
        />
    </>
}

export { Create }
