import React, { useState } from 'react'

import { IconButton } from '@mui/material';
import ModeEditIcon from '@mui/icons-material/ModeEdit';
import DoDisturbOnIcon from '@mui/icons-material/DoDisturbOn';
import CheckIcon from '@mui/icons-material/Check';
import EventAvailableIcon from '@mui/icons-material/EventAvailable';

import { ModalDialog } from '../../../components/modalDialog';

import { useToDoContext } from '../../../providers/toDo';
import types from '../../../providers/types';

import { Notification } from '../../../utils/notification';

import { stateTypes } from './enum';
import { UpdateDescription } from './update';

const Modify = props => {
    const { cell } = props
    const [modifyModal, setModifyModal] = useState(false)

    const closeModifyModal = () => setModifyModal(false)

    return <>
        <IconButton 
            aria-label="Modify"
            onClick={() => setModifyModal(true)}
        >
            <ModeEditIcon />
        </IconButton>
        <ModalDialog
            open= {modifyModal}
            handleClose={() => closeModifyModal}
            title= "Updating TO-DO"
            content= {<UpdateDescription cell={cell} closeModal={closeModifyModal}/>}
        />
    </>
}

const Canceled = props => {
    const { cell } = props
    const { dispatch } = useToDoContext()
    const [openNotification, setOpenNotification] = useState(false)
    const [typeNotification] = useState('error')
    const [mssgNotification, setMssgNotification] = useState('process err')

    const submit = () => {
        if(cell.state !== stateTypes.TODO) {
            setMssgNotification('task cannot be cancelled')
            setOpenNotification(true)
            return
        }

        const todo = {
            ...cell,
            state: stateTypes.CANCELD
        }

        dispatch({
            type: types.updateStateToDo,
            payload: todo
        })

    }

    return <>
        <IconButton 
            aria-label="Canceled"
            onClick={submit}
        >
            <DoDisturbOnIcon />
        </IconButton>
        <Notification 
            openAlert= {openNotification} 
            setOpenAlert= {setOpenNotification}
            type= {typeNotification}
            mssg= {mssgNotification}
        />
    </>
}

const Complete = props => {
    const { cell } = props
    const { dispatch } = useToDoContext()
    const [openNotification, setOpenNotification] = useState(false)
    const [typeNotification] = useState('error')
    const [mssgNotification, setMssgNotification] = useState('process err')


    const submit = () => {
        if(cell.state !== stateTypes.TODO) {
            setMssgNotification('task cannot be complete')
            setOpenNotification(true)
            return
        }

        const todo = {
            ...cell,
            state: stateTypes.FINISHED
        }

        dispatch({
            type: types.updateStateToDo,
            payload: todo
        })

    }

    return <>
        <IconButton 
            aria-label="Complete"
            onClick={submit}
        >
            <CheckIcon />
        </IconButton>
        <Notification 
            openAlert= {openNotification} 
            setOpenAlert= {setOpenNotification}
            type= {typeNotification}
            mssg= {mssgNotification}
        />
    </>
}

const Enable = props => {
    const { cell } = props
    const { dispatch } = useToDoContext()
    const [openNotification, setOpenNotification] = useState(false)
    const [typeNotification] = useState('error')
    const [mssgNotification, setMssgNotification] = useState('process err')


    const submit = () => {
        if(cell.state === stateTypes.TODO) {
            setMssgNotification('task cannot be enable')
            setOpenNotification(true)
            return
        }

        const todo = {
            ...cell,
            state: stateTypes.TODO
        }

        dispatch({
            type: types.updateStateToDo,
            payload: todo
        })

    }

    return <>
        <IconButton 
            aria-label="Enable"
            onClick={submit}
        >
            <EventAvailableIcon />
        </IconButton>
        <Notification 
            openAlert= {openNotification} 
            setOpenAlert= {setOpenNotification}
            type= {typeNotification}
            mssg= {mssgNotification}
        />
    </>
}

export {
    Modify,
    Canceled,
    Complete,
    Enable
}
