import React from 'react'

import { useForm } from 'react-hook-form';
import { styled, Button } from '@mui/material';
import { TexfieldValidation } from '../../../components/textFieldValidation';
import { useToDoContext } from '../../../providers/toDo';
import types from '../../../providers/types';

const InputButton = styled(Button)({
    marginTop: '16px',
})

const UpdateDescription = ({cell, closeModal}) => {
    const { register, handleSubmit, reset, formState: { errors } } = useForm();
    const { dispatch } = useToDoContext()

    const toDoShema = {
        description: {
            required: { value: true, message: 'Description required' },
            minLength: { value: 2, message: 'Should have a min of 2 characters' },
            maxLength:{ value: 300, message: 'Should have a max of 300 characters'}
        }
    };

    const onSubmit = data => {
        const todo = {
            ...cell,
            description:data.description
        }

        dispatch({
            type: types.modifyToDo,
            payload: todo
        })

        reset();
        closeModal()
    }

    return <form onSubmit={handleSubmit(onSubmit)}>
        <TexfieldValidation
            required
            id= "description"
            label= "Description"
            shemaValidation= {toDoShema}
            errors= {errors}
            register= {register}
        />
        <InputButton size="small" variant="outlined" color="success" type="submit" >SET</InputButton>
    </form>
}

export { UpdateDescription }