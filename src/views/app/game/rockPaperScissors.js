import React, { useState } from 'react'
import { styled, Button, Typography, Box, FormControl, InputLabel, Select, MenuItem } from '@mui/material';

const SetBox = styled(Box)({
    marginTop: '16px',
})

const HistoryBox = styled(Box)({
    width: '100%',
    height: '100%',

    display: 'flex',
    flexDirection: 'column',
    justifyContent:'center',
    alignItems: 'center'
})

const GameBox = styled(Box)({

    display: 'flex',
    flexDirection: 'row',
    justifyContent:'center',
    alignItems: 'center'
})

const StatisticsTypography = styled(Typography)({
    marginLeft:'16px' 
})

const RockPaperScissors = () => {
    const [livesPlayer, setLivesPlayer] = useState(3)
    const [livesPc, setLivesPc] = useState(3)
    const [playerMove, setPlayerMove] = useState('')
    // eslint-disable-next-line no-unused-vars
    const [PCMove, setPCMove] = useState('')
    const [gameHistory, setGameHistory] = useState([])

    const resultMap = new Map()
    resultMap.set(1,'Tie')
    resultMap.set(2,'Winner')
    resultMap.set(3,'Loser')

    const moveMap = new Map()
    moveMap.set(1,'Rock')
    moveMap.set(2,'Paper')
    moveMap.set(3,'Scissors')

    const moveEnum = {
        'Rock': 1,
        'Paper': 2,
        'Scissors': 3
    }

    const playerGameStatus = {
        'Tie': 1,
        'Winner': 2,
        'Loser': 3
    }

    const handleChange = (event) => {
        setPlayerMove(event.target.value);
    };

    const setLives = (statusPlayerGame) => {
        if(statusPlayerGame === playerGameStatus.Tie)
            return;
        if(statusPlayerGame === playerGameStatus.Winner)
            setLivesPc(livesPc-1)
        if(statusPlayerGame === playerGameStatus.Loser)
            setLivesPlayer(livesPlayer-1)

    }

    const pcPlayRock = playerMove => {
        switch(playerMove) {
            case moveEnum.Rock:{
                const statistics = {
                    pcPlay: moveEnum.Rock,
                    playerPlay: moveEnum.Rock,
                    playerResult:playerGameStatus.Tie
                }
                setGameHistory([...gameHistory, statistics])
                setLives(playerGameStatus.Tie)
                return
            }
            case moveEnum.Paper:{
                const statistics = {
                    pcPlay: moveEnum.Rock,
                    playerPlay: moveEnum.Paper,
                    playerResult:playerGameStatus.Winner
                }
                setGameHistory([...gameHistory, statistics])
                setLives(playerGameStatus.Winner)
                return
            }
            case moveEnum.Scissors:{
                const statistics = {
                    pcPlay: moveEnum.Rock,
                    playerPlay: moveEnum.Scissors,
                    playerResult:playerGameStatus.Loser
                }
                setGameHistory([...gameHistory, statistics])
                setLives(playerGameStatus.Loser)
                return
            }
            default:
                return 'err';
        }
    }
    
    const pcPlayPaper = playerMove => {
        switch(playerMove) {
            case moveEnum.Rock:{
                const statistics = {
                    pcPlay: moveEnum.Paper,
                    playerPlay: moveEnum.Rock,
                    playerResult:playerGameStatus.Loser
                }
                setGameHistory([...gameHistory, statistics])
                setLives(playerGameStatus.Loser)
                return
            }
            case moveEnum.Paper:{
                const statistics = {
                    pcPlay: moveEnum.Paper,
                    playerPlay: moveEnum.Paper,
                    playerResult:playerGameStatus.Tie
                }
                setGameHistory([...gameHistory, statistics])
                setLives(playerGameStatus.Tie)
                return
            }
            case moveEnum.Scissors:{
                const statistics = {
                    pcPlay: moveEnum.Paper,
                    playerPlay: moveEnum.Scissors,
                    playerResult:playerGameStatus.Winner
                }
                setGameHistory([...gameHistory, statistics])
                setLives(playerGameStatus.Winner)
                return
            }
            default:
                return 'err';
        }
    }
    
    const pcPlayScissors = playerMove => {
        switch(playerMove) {
            case moveEnum.Rock:{
                const statistics = {
                    pcPlay: moveEnum.Scissors,
                    playerPlay: moveEnum.Rock,
                    playerResult:playerGameStatus.Winner
                }
                setGameHistory([...gameHistory, statistics])
                setLives(playerGameStatus.Winner)
                return
            }
            case moveEnum.Paper:{
                const statistics = {
                    pcPlay: moveEnum.Scissors,
                    playerPlay: moveEnum.Paper,
                    playerResult:playerGameStatus.Loser
                }
                setGameHistory([...gameHistory, statistics])
                setLives(playerGameStatus.Loser)
                return
            }
            case moveEnum.Scissors:{
                const statistics = {
                    pcPlay: moveEnum.Scissors,
                    playerPlay: moveEnum.Scissors,
                    playerResult:playerGameStatus.Tie
                }
                setGameHistory([...gameHistory, statistics])
                setLives(playerGameStatus.Tie)
                return
            }
            default:
                return 'err';
        }
    }

    const start = () => {
        const random =  Math.round(Math.random() * 3)
        const pcMove = random === 0 ? 1 : random
        setPCMove(pcMove)

        switch(pcMove) {
            case moveEnum.Rock:{
                pcPlayRock(playerMove)
                return
            }
            case moveEnum.Paper:{
                pcPlayPaper(playerMove)
                return
            }
            case moveEnum.Scissors:{
                pcPlayScissors(playerMove)
                return
            }
            default:
                return 'err';
        }


    }
    

    return <SetBox>
        <Typography>RockPaperScissors</Typography>
        <Typography>{`livesPlayer: ${livesPlayer}`}</Typography>
        <Typography>{`livesPc: ${livesPc}`}</Typography>
        <Button>Reload</Button>
        <hr/>
        {
            (livesPlayer < 1 || livesPc < 1)
            ?   
                livesPc > livesPlayer 
                    ?   <Typography>WINNER PC</Typography>
                    :   <Typography>WINNER PLAYER</Typography>
            :   <>
                    <Typography>Select option</Typography>
                    <Box sx={{ minWidth: 120 }}>
                        <FormControl sx={{ minWidth: 120 }}>
                            <InputLabel id="demo-simple-select-label">MOVE</InputLabel>
                            <Select
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            value={playerMove}
                            label="playerMove"
                            onChange={handleChange}
                            >
                                <MenuItem value={moveEnum.Rock}>Rock</MenuItem>
                                <MenuItem value={moveEnum.Paper}>Paper</MenuItem>
                                <MenuItem value={moveEnum.Scissors}>Scissors</MenuItem>
                            </Select>
                        </FormControl>
                    </Box>
                    <Button onClick={start}>Play</Button>
            </>
        }
        <hr/>
        <HistoryBox>
            {gameHistory.map((game, idx) => {
                return <GameBox key={idx} >
                    <StatisticsTypography>{`The pc plays: ${moveMap.get(game.pcPlay)}`}</StatisticsTypography>
                    <StatisticsTypography>{`The player plays: ${moveMap.get(game.playerPlay)}`}</StatisticsTypography>
                    <StatisticsTypography>{`Result: ${resultMap.get(game.playerResult)}`}</StatisticsTypography>
                </GameBox>
            })}
        </HistoryBox>
        
    </SetBox>
}

export default RockPaperScissors